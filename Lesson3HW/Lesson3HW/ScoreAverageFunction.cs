﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3HW
{
    public class AverageScore
    {

        public double Score (double literatureScore, double mathScore, double englishScore)

        {
            return (literatureScore + mathScore + englishScore) / 3;
        }
    }
}
