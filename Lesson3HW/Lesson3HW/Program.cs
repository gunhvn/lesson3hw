﻿// See https://aka.ms/new-console-template for more information
using Lesson3HW;
using System.Threading.Channels;

Console.WriteLine(" Average Score Calculation Program");
Console.WriteLine(" Let's input your name: ");
string yourName = (string)Console.ReadLine();

Console.WriteLine(" Your literature score: ");
double litterature = double.Parse(Console.ReadLine());
Console.WriteLine(" Your math score: ");
double math = double.Parse(Console.ReadLine());
Console.WriteLine(" Your english score: ");
double english = double.Parse(Console.ReadLine());

AverageScore Result = new AverageScore();

Console.WriteLine("====================================================");
Console.WriteLine($"Student name: {yourName} |   Average Score: {Result.Score(litterature,math,english)}");

Console.ReadLine();